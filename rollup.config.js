import buble from 'rollup-plugin-buble'
import {
  uglify
} from 'rollup-plugin-uglify'

export default {
  input: 'src/sourcedata.js',
  output: {
    format: 'umd',
    name: 'sourcedata',
    file: 'dist/sourcedata.js',
    amd: {
      id: 'sourcedata'
    }
  },
  plugins: [buble(), uglify()]
}
