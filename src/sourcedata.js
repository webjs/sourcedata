export default class SourceData {
	constructor(opt = {}) {
		this._source = []
		this._data = []
		this._view = []

		this._size = 0
		this._pages = 1
		this._page = 1
		this._prev = 0
		this._next = 0

		this._filter = null
		this._sort = null

		this.setOpt(opt)
	}

	setOpt(opt) {
		const prevent = ['data', 'view', 'pages', 'prev', 'next']

		prevent.forEach(k => {
			if (opt.hasOwnProperty(k))
				delete opt[k]
		})

		if (opt.filter) {
			this._filter = opt.filter
			delete opt.filter
		}

		if (opt.sort) {
			this._sort = opt.sort
			delete opt.sort
		}

		for (let o in opt)
			if (opt.hasOwnProperty(o))
				this[o] = opt[o]
  }

  setData(data) {
    this.source = data
  }

	// Getters and Setters
	get source() {
		return this._source
	}
	set source(arr = []) {
		if (Array.isArray(arr)) {
			this._source = arr
			this._data = arr.slice()

			if (this._filter)
				this._data = this.data.filter(this._filter)

			if (this._sort)
				this._data = this.data.sort(this._sort)

			if (this.size)
				this.paginate(this.size)
			else
				this._view = this._data
		}
	}

	get data() {
		return this._data
	}

	get view() {
		return this._view
	}

	get size() {
		return this._size
	}
	set size(i) {
		if (!isNaN(i)) {
			this._size = parseInt(i)
			this._data = this.source.slice()

			if (this._filter)
				this._data = this.data.filter(this._filter)

			if (this._sort)
				this._data = this.data.sort(this._sort)

			this.paginate(this._size)
		}
	}

	get total() {
		return this._data.length
	}

	get pages() {
		return this._pages
	}

	get page() {
		return this._page
	}
	set page(i) {
		if (!isNaN(i))
			if (i > this._pages) {
				return this.page = this._pages
			} else {
				const page = parseInt(i) || 1,
					view = page ? page - 1 : 0,
					start = view * this._size,
					end = start + this._size

				this._page = page
				this._prev = page - 1
				this._next = page < this._pages ? page + 1 : 0
				return this._view = this._data.slice(start, end)
			}
	}

	get prev() {
		return this._prev
	}

	get next() {
		return this._next
	}

	// Methods
	paginate(i) {
		if (this._size !== i)
			return this.size = i
		this._pages = Math.ceil(this._data.length / i)
		return this.page = 1
	}

	pgPrev() {
		this.page = this._prev
	}

	pgNext() {
		this.page = this._next
	}

	pgFirst() {
		this.page = 1
	}

	pgLast() {
		this.page = this._pages
	}

	items(m, a) {
		const items = this.source.slice(),
			fn = a ? a instanceof Function : null

		switch (m) {
			case 'filter':
				this._filter = fn ? a : null
				return this.size = this.size

			case 'sort':
				this._sort = fn ? a : null
				return this.size = this.size

			case 'get':
				if (!fn)
					return null

				let i = items.findIndex(a),
					item = items[i]

				return {
					index: i,
					value: item
				}

			case 'set':
				items[a.index] = a.value
				this.source = items
				return a

			case 'add':
				items.push(a)
				this.source = items
				return a

			case 'del':
				items.splice(a, 1)
				this.source = items
				return a

			default:
				return null
		}
	}

	filter(fn) {
		return this.items('filter', fn)
	}

	sort(fn) {
		return this.items('sort', fn)
	}

	getItem(fn) {
		return this.items('get', fn)
	}

	setItem(item) {
		return this.items('set', item)
	}

	addItem(item) {
		return this.items('add', item)
	}

	delItem(index) {
		return this.items('add', index)
	}
}
