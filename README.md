# @jsweb/sourcedata
Simple dynamic JS datasource object, useful to combine with observables like Vue

***

## Installation

You can install it with NPM, Yarn or via Unpkg CDN

### CDN
```html
<script src="https://unpkg.com/@jsweb/sourcedata"></script>
```

## Usage

It's possible to use **sourcedata** with module loaders or globally.

### ES6

```javascript
import SourceData from '@jsweb/sourcedata'
```

### CommonJS

```javascript
let SourceData = require('@jsweb/sourcedata')
```

### CDN

If you install it via CDN, `sourcedata` object will be available at global window scope.

## Instance

**sourcedata** contains a class object that can be instantiated to a variable.

```javascript
let sdata = new SourceData(opt)
```

An `opt` object can setup SourceData as follows (default values):

```javascript
let sdata = new SourceData({
	source: [],	// Array with initial data.
	size: 0	    // Page size, items per page. 0 = no pagination
})

// Or simply: let sdata = new SourceData
```

## Properties

- **total**: total data items count
- **size**: the page size, items per page
- **pages**: pages count
- **page**: current page number
- **data**: result data items (filtered and/or sorted, if any)
- **view**: current page items
- **prev**: previews page number (or 0 if none)
- **next**: next page number (or 0 if none)

## Methods

- **setData(arr)**: set a new Array data to instance
- **pgPrev()**: navigates to previews page
- **pgNext()**: navigates to next page
- **pgFirst()**: navigates to first page
- **pgLast()**: navigates to last page
- **filter(fn)**: native JS Array.filter method to filter data (opt in)
- **sort(fn)**: native JS Array.sort method to sort data (opt in)
- **getItem(fn)**: gets an item with native JS Array.find method, returns { index: i, value: item }
- **setItem(obj)**: replaces an item value, obj argument needs to be like { index: i, value: item }
- **addItem(item)**: adds new item to data source using native JS Array.push
- **delItem(index)**: deletes an item from data source using native JS Array.splice
- **items(method, arg)**: executes a `method` with `arg`, `method` can be `filter`, `sort`, `get`, `set`, `add`, `del`,

## TODO

- ~~Filter/Sort items~~
- ~~Get/Set items~~
- ~~Add/Del items~~
- Observable events
